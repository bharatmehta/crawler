# Crawler

# Build

``
mvn clean install
``

#Run

``
java -jar crawler-0.1-SNAPSHOT.jar
``
# Tool Stack

- Java 8
- Spring Boot (For moving fast)
- JSoup (Java Scrapping library, for moving fast)
- JUnit 
- Maven

# TBD

- More testing
- min.js, min-1.0.js , min.v1.js, min.js?v=1 are all different

# Issues Faced

- Lack of time
- This is not a smart way of scrapping Google Search Results.We should be using Google Search API but that costs money :(
- JSoup didn't work for some sites. Had to ask google .
