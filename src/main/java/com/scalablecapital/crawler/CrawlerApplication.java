package com.scalablecapital.crawler;

import com.scalablecapital.crawler.component.Crawler;
import com.scalablecapital.crawler.exception.CrawlerException;
import com.scalablecapital.crawler.exception.GoogleSearchException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class CrawlerApplication implements CommandLineRunner {

  @Autowired
  private Crawler crawler;


  public static void main(String[] args) {
    SpringApplication.run(CrawlerApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    try (Scanner input = new Scanner(System.in)) {
      String line;

      System.out.println("Enter keyword to findLinks on www.google.com: ");

      while (!(line = input.nextLine()).isEmpty()) {
        try {
          List<Future<Set<String>>> futureList = crawler.crawl(line);
          print(futureList);
        } catch (GoogleSearchException e) {
          log.error(e.getMessage(), e);
          System.exit(e.getCode());
        } catch (CrawlerException e) {
          log.error(e.getMessage(), e);
          System.exit(e.getCode());
        }
      }

      System.exit(0);
    }
  }


  private void print(final List<Future<Set<String>>> futureList) {
    List<String> libs = new ArrayList<>();

    futureList.stream()
        .forEach(future -> {

          try {
            libs.addAll(future.get(5, TimeUnit.SECONDS));
          } catch (ExecutionException | InterruptedException e) {
            log.error("Error in getting results", e);
          } catch (TimeoutException e) {
            log.error("Error in getting results", e);
          }

        });

    System.out.println("Top 5 .js libraries found so far are:");

    libs.stream()
        .collect(
            Collectors.groupingBy(Function.identity(), Collectors.counting())
        )
        .entrySet()
        .stream()
        .sorted(Map.Entry.<String, Long>comparingByValue().reversed())
        .limit(5)
        .forEach(System.out::println);
  }

  public ExecutorService executorService() {
    return Executors.newFixedThreadPool(10, new ThreadFactory() {

      @Override
      public Thread newThread(Runnable r) {
        Thread t = new Thread(r, "Crawler");
        t.setUncaughtExceptionHandler(exceptionHandler());
        return t;
      }
    });
  }

  public UncaughtExceptionHandler exceptionHandler() {
    return new Thread.UncaughtExceptionHandler() {
      @Override
      public void uncaughtException(Thread t, Throwable e) {
        LoggerFactory.getLogger(t.getName()).error(e.getMessage(), e);
      }
    };
  }

}

