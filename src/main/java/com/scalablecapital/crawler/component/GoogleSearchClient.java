package com.scalablecapital.crawler.component;

import com.scalablecapital.crawler.exception.GoogleSearchException;
import com.scalablecapital.crawler.util.Extractor;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Set;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

@Slf4j
public class GoogleSearchClient {

  private String url;

  private final int count;

  private final Extractor googleSearchResultExtractor;

  public GoogleSearchClient(@NonNull String url, @NonNull Extractor googleSearchResultExtractor) {
    this.url = url;
    this.count = 500;
    this.googleSearchResultExtractor = googleSearchResultExtractor;
  }

  public Set<String> findLinks(@NonNull final String keyword) {
    try {
      final String SEARCH_URL = searchURL(keyword, count);
      log.info("Hitting : {}", SEARCH_URL);

      Document doc = Jsoup.connect(SEARCH_URL)
          .userAgent(
              "Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36")
          .timeout(10000)
          .get();

      return Collections.unmodifiableSet(googleSearchResultExtractor.extract(doc));
    } catch (IOException e) {
      throw new GoogleSearchException("Problem occurred while searching for " + keyword, e);
    }

  }

  private String searchURL(final String keyword, int count) throws UnsupportedEncodingException {
    return url.concat(encode(keyword)).concat("&num=").concat(Integer.toString(count));
  }

  private String encode(String keyword) throws UnsupportedEncodingException {
    return URLEncoder.encode(keyword, StandardCharsets.UTF_8.name());
  }

}
