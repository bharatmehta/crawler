package com.scalablecapital.crawler.component;

import com.scalablecapital.crawler.exception.CrawlerException;
import com.scalablecapital.crawler.exception.GoogleSearchException;
import com.scalablecapital.crawler.util.AsyncJavascriptLibraryExtractor;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Slf4j
public class Crawler {

  private final GoogleSearchClient googleSearchClient;

  private final ThreadPoolTaskExecutor taskExecutor;


  public List<Future<Set<String>>> crawl(final String keyword) {
    log.info("Searching for: {}", keyword);
    List<Future<Set<String>>> futureList = new ArrayList<>();

    try {
      Set<String> searchResults = googleSearchClient.findLinks(keyword);
      final int resultCount = searchResults.size();

      log.info("Number of Unique Search Result " + resultCount);

      if (resultCount > 0) {

        log.info("Creating " + resultCount + " jobs");
        CountDownLatch countDownLatch = new CountDownLatch(resultCount);

        searchResults.stream()
            .forEach(result -> {
              futureList.add(
                  taskExecutor.submit(new AsyncJavascriptLibraryExtractor(result, countDownLatch)));
            });

        countDownLatch.await(3, TimeUnit.MINUTES);
        log.info("Number of jobs remaining: " + countDownLatch.getCount());

      }
    } catch (GoogleSearchException e) {
      throw e;
    } catch (InterruptedException e) {
      throw new CrawlerException("Problem occured", e);
    }
    return futureList;

  }


}
