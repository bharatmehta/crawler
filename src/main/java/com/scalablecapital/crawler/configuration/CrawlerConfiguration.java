package com.scalablecapital.crawler.configuration;

import com.scalablecapital.crawler.component.GoogleSearchClient;
import com.scalablecapital.crawler.util.GoogleSearchResultExtractor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class CrawlerConfiguration {

  @Value("${googlesearch.url}")
  private String url;

  @Bean
  public GoogleSearchClient googleSearchClient() {
    return new GoogleSearchClient(url, new GoogleSearchResultExtractor());
  }


  @Bean
  public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(5);
    executor.setMaxPoolSize(5);
    executor.setThreadNamePrefix("Crawler");
    executor.initialize();
    return executor;
  }
}
