package com.scalablecapital.crawler.util;

import java.util.Set;
import java.util.stream.Collectors;
import lombok.NonNull;
import org.jsoup.nodes.Document;

/**
 * Extracts unique names of javascript libraries
 */
public class JavascriptLibraryExtractor implements Extractor {

  @Override
  public Set<String> extract(@NonNull final Document document) {
    return document.getElementsByTag("script")
        .stream()
        .filter(e -> e.hasAttr("src"))
        .map(e -> e.attr("src"))
        .filter(name -> name.endsWith(".js"))
        .map(String::trim)
        .map(this::libraryName)
        .filter(i -> i != null && i.trim().length() > 0)
        .collect(Collectors.toSet());
  }

  /**
   * This method extracts the name of scripts from URLs and removes query parameters if any at the
   * end
   *
   * @return String
   */
  private String libraryName(final String urlString) {
    String libraryName = null;
    if (urlString != null && urlString.trim().length() > 0) {
      String[] segments = urlString.trim().split("/");
      libraryName = segments[segments.length - 1];
      int index = libraryName.indexOf("?");
      if (index > 0) {
        libraryName = libraryName.substring(0, index);
      }
    }

    return libraryName;
  }

}
