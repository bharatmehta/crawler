package com.scalablecapital.crawler.util;

import java.util.Set;
import java.util.stream.Collectors;
import lombok.NonNull;
import org.jsoup.nodes.Document;

/**
 * Parses Google Search page and returns a Set of Links
 */
public class GoogleSearchResultExtractor implements Extractor {

  @Override
  public Set<String> extract(@NonNull Document document) {

    final String hrefPrefix = "/url?q=";

    return document.select("a[href]")
        .stream()
        .map(e -> e.attr("href"))
        .filter(e -> e.startsWith("http://") || e.startsWith("https://"))
        .map(e -> e.replace(hrefPrefix, ""))
        .collect(Collectors.toSet());

  }
}
