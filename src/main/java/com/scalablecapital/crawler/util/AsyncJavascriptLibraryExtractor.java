package com.scalablecapital.crawler.util;

import com.scalablecapital.crawler.exception.CrawlerException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Asyn version for {@link JavascriptLibraryExtractor}
 */
@AllArgsConstructor
@Slf4j
public class AsyncJavascriptLibraryExtractor extends JavascriptLibraryExtractor
    implements Callable<Set<String>> {

  private final String url;

  private final CountDownLatch latch;

  /**
   * Computes a result, or throws an exception if unable to do so.
   *
   * @return computed result
   * @throws Exception if unable to compute a result
   */
  @Override
  public Set<String> call() {
    Set<String> libraryNames = new HashSet<>();

    try {
      Document doc = parse();

      libraryNames.addAll(extract(doc));
    } catch (MalformedURLException e) {
      log.error("Problem reading {}", url, e);
    } catch (HttpStatusException e) {
      log.error("HTTP Status: {} on hitting {}", e.getStatusCode(), url, e);
    } catch (IOException e) {
      log.error("Problem occured while reading {}", url, e);
      throw new CrawlerException("Problem while reading " .concat(url), e);
    } finally {
      //Irrespective of the status of the job, the lath should go down
      latch.countDown();
    }

    return Collections.unmodifiableSet(libraryNames);
  }


  public Document parse() throws IOException {
    return Jsoup.connect(url)
        .sslSocketFactory(socketFactory())
        .userAgent(
            "Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36")
        .referrer("https://www.google.com")
        .timeout(10000)
        .get();
  }


  public String getUrl() {
    return url;
  }

  //Disable checking of certificates on SSL sites
  private SSLSocketFactory socketFactory() {
    TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
      public java.security.cert.X509Certificate[] getAcceptedIssuers() {
        return null;
      }

      public void checkClientTrusted(X509Certificate[] certs, String authType) {
      }

      public void checkServerTrusted(X509Certificate[] certs, String authType) {
      }
    }};

    try {
      SSLContext sslContext = SSLContext.getInstance("TLS");
      sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

      return sslContext.getSocketFactory();
    } catch (NoSuchAlgorithmException | KeyManagementException e) {
      throw new RuntimeException("Failed to create a SSL socket factory", e);
    }
  }


}
