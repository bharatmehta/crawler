package com.scalablecapital.crawler.util;

import java.util.Set;
import lombok.NonNull;
import org.jsoup.nodes.Document;

/**
 * Parses {@link Document}  and returns  unique values
 */
public interface Extractor {

  /**
   * Parses a DOM and returns unique values
   * @param document DOM
   * @return Set
   */
  Set<String> extract(@NonNull final Document document);
}
