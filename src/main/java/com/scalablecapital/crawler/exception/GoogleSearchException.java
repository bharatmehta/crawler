package com.scalablecapital.crawler.exception;

public class GoogleSearchException extends RuntimeException {

  private final int code = 1;

  public GoogleSearchException(String message, Throwable e) {
    super(message, e);
  }

  public int getCode() {
    return code;
  }
}
