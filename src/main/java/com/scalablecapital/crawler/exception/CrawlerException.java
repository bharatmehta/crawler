package com.scalablecapital.crawler.exception;

public class CrawlerException extends RuntimeException {

  private final int code = 2;

  public CrawlerException(String message, Throwable e) {
    super(message, e);
  }

  public int getCode() {
    return code;
  }
}
