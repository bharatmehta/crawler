package com.scalablecapital.crawler.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JavascriptLibraryExtractorTest {

  private JavascriptLibraryExtractor extractor;

  @BeforeEach
  public void setup() {
    extractor = new JavascriptLibraryExtractor();
  }

  @Test
  public void shouldReturnEmpty() throws IOException {
    File input = new File("src/test/resources/Germany.htm");
    Document document = Jsoup.parse(input, "UTF-8", "http://www.wikipedia.org/");

    Set<String> libNames = extractor.extract(document);
    assertTrue(libNames.isEmpty());
  }

  @Test
  public void shouldReturnNames() throws IOException {
    File input = new File("src/test/resources/jQuery.htm");
    Document document = Jsoup.parse(input, "UTF-8", "http://www.w3cschools.com/");

    Set<String> libNames = extractor.extract(document);
    assertEquals(1, libNames.size());
    assertTrue(libNames.contains("jquery.min.js"));
    libNames.stream()
        .forEach(System.out::println);
  }

  @Test
  public void shouldThrowException() {
    assertThrows(NullPointerException.class, () -> extractor.extract(null));
  }

}
