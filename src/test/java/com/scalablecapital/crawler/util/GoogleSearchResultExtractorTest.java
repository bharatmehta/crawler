package com.scalablecapital.crawler.util;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GoogleSearchResultExtractorTest {

  private GoogleSearchResultExtractor extractor;

  @BeforeEach
  public void setup() {
    extractor = new GoogleSearchResultExtractor();
  }

  @Test
  public void shouldReturnLinks() throws IOException {
    File input = new File("src/test/resources/Germany.htm");
    Document document = Jsoup.parse(input, "UTF-8", "http://www.wikipedia.org/");

    Set<String> links = extractor.extract(document);
    assertFalse(links.isEmpty());
    links.stream()
        .forEach(System.out::println);
  }

  @Test
  public void shouldReturnNoLinks() throws IOException {
    File input = new File("src/test/resources/jQuery.htm");
    Document document = Jsoup.parse(input, "UTF-8", "http://www.w3cschools.com/");

    Set<String> links = extractor.extract(document);
    assertTrue(links.isEmpty());
  }

  @Test
  public void shouldThrowException() {
    assertThrows(NullPointerException.class, () -> extractor.extract(null));
  }
}
