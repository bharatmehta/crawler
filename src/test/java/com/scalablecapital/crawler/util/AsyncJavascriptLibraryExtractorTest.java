package com.scalablecapital.crawler.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AsyncJavascriptLibraryExtractorTest {

  private AsyncJavascriptLibraryExtractor extractor;

  private CountDownLatch latch;

  @BeforeEach
  public void setup() {
    latch = new CountDownLatch(1);
    extractor = new AsyncJavascriptLibraryExtractor("http://www.stackoverflow.com", latch);

  }

  @Test
  public void shouldReturnEmpty() throws IOException {
    File input = new File("src/test/resources/Germany.htm");
    Document document = Jsoup.parse(input, "UTF-8", "http://www.wikipedia.org/");

    Set<String> libNames = extractor.extract(document);
    assertTrue(libNames.isEmpty());
  }

  @Test
  public void shouldReturnNamesWithDocument() throws IOException {
    File input = new File("src/test/resources/jQuery.htm");
    Document document = Jsoup.parse(input, "UTF-8", "http://www.w3cschools.com/");

    Set<String> libNames = extractor.extract(document);

    assertEquals(1, libNames.size());
    assertTrue(libNames.contains("jquery.min.js"));

    libNames.stream()
        .forEach(System.out::println);
  }

  @Test
  public void shouldReturnNames() throws IOException {
    Set<String> libNames = extractor.call();

    assertEquals(0, latch.getCount());
    assertEquals("http://www.wikipedia.org/", extractor.getUrl());
    assertFalse(libNames.isEmpty());

    libNames.stream()
        .forEach(System.out::println);
  }

  @Test
  public void shouldThrowException() {
    assertThrows(NullPointerException.class, () -> extractor.extract(null));
  }

}
