package com.scalablecapital.crawler.component;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.scalablecapital.crawler.util.GoogleSearchResultExtractor;
import java.io.IOException;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class GoogleSeachTest {

  private GoogleSearchClient googleSearch;

  @BeforeEach
  public void setup() {
    googleSearch = new GoogleSearchClient("https://www.google.com/search?q=",
        new GoogleSearchResultExtractor());
  }

  @Test
  public void shouldPassWithResults() throws IOException {
    Set<String> searchResults = googleSearch.findLinks("stackoverflow");

    assertNotNull(searchResults);
    assertFalse(searchResults.isEmpty());

    searchResults.stream()
        .forEach(result -> {
          System.out.println(result);
          assertNotNull(result);
        });
  }

}
