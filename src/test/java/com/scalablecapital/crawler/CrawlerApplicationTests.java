package com.scalablecapital.crawler;

import com.scalablecapital.crawler.configuration.TestApplicationConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TestApplicationConfiguration.class)
public class CrawlerApplicationTests {

  @Test
  public void contextLoads() {

  }

}
